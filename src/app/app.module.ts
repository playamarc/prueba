import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Movinet1Component } from './components/movinet/movinet1.component';
import { Movinet2Component } from './components/movinet/movinet2.component';
import { APP_ROUTING } from './app_routers.routing';


@NgModule({
  declarations: [
    AppComponent,
    Movinet1Component,
    Movinet2Component
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
