import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-movinet1',
  templateUrl: './movinet1.component.html'
})
export class Movinet1Component implements OnInit {

  constructor( private ruta: Router) { }

  ngOnInit() {
  }
 // La clase Router se usa cuando no necesitamos enviar parametros
 agregarPersona() {
   this.ruta.navigate( ['2']);
 }
}
