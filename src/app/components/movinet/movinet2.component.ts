import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-movinet2',
  templateUrl: './movinet2.component.html'
})
export class Movinet2Component implements OnInit {

  constructor( private activarRuta: ActivatedRoute) {
    this.activarRuta.params.subscribe( parametros => {
      console.log ( parametros );
      // Para ver el parámetro id podria ser, esto porque en la ruta mandamos el id:
      console.log ( parametros['id']);
    });
   }

  ngOnInit() {
  }

}
