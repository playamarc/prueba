import { NgTools_InternalApi_NG_2_LazyRouteMap } from '@angular/compiler-cli/src/ngtools_api';
import { Routes, RouterModule } from '@angular/router';
import { Movinet1Component } from './components/movinet/movinet1.component';
import { Movinet2Component } from './components/movinet/movinet2.component';
import { NgModule } from '@angular/core';
import { routerNgProbeToken } from '../../node_modules/@angular/router/src/router_module';
/*
const routes: Routes = [
  { path: '1', component: Movinet1Component },
  { path: '2', component: Movinet2Component },
];

export const App_routersRoutes = RouterModule.forChild(routes);

@NgModule({
  imports: [RouterModule.forRoot(App_routersRoutes)],
  exports: [RouterModule]
})*/



const APP_ROUTES: Routes = [
  { path: '1', component: Movinet1Component },
  { path: '2/:id', component: Movinet2Component },
  { path: '2', component: Movinet2Component },
  { path: '**', pathMatch: 'full', redirectTo: '1' },
];
@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class APP_ROUTING {
}


